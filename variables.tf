variable "envs" {
  type = list(object({
    enable_dev_approle = bool
    name               = string
    jwt_bound_claims   = map(any)
  }))
  description = "List of environment names and bound claims"
}

variable "gitlab_vault_jwt_project_id" {
  type        = number
  description = "The gitlab project that will be granted vault access through JWT auth."
}

variable "project_name" {
  type        = string
  description = "Project name will be used as a prefix in all Vault resources (just like 'env')"
}

variable "read_secrets" {
  type = list(object({
    name        = string
    description = string
  }))
  default     = []
  description = <<EOT
  A list of (kv2) secrets that this module will auto generate READ policies for.
  Project name and ENV will be added to those secrets names.
  EOT
}

variable "transit_keys" {
  type = list(object({
    name             = string
    description      = string
    deletion_allowed = bool
  }))
  default     = []
  description = <<EOT
  A list of transit engine keys that this module will auto generate encrypt/decrypt policies for.
  Project name and ENV will be added to those key names.
  EOT
}
