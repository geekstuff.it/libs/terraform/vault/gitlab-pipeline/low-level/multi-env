terraform {
  required_version = "~> 1.2"
}

module "multi" {
  source = "git::https://gitlab.com/geekstuff.it/libs/terraform/vault/gitlab-pipeline/low-level/single-env?ref=v0.0.3"

  for_each = {
    for obj in var.envs : obj.name => obj
  }

  env                         = each.value.name
  enable_dev_approle          = each.value.enable_dev_approle
  jwt_bound_claims            = each.value.jwt_bound_claims
  gitlab_vault_jwt_project_id = var.gitlab_vault_jwt_project_id
  project_name                = var.project_name
  read_secrets                = var.read_secrets
  transit_keys                = var.transit_keys
}
